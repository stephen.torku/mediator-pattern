public class Flight implements Action{

    private WatchTowerInterface watchTower;
    private String name;

    Flight(WatchTowerInterface watchTower, String name){
        this.watchTower=watchTower;
        this.name = name;
    }

    @Override
    public void land(){
        if (watchTower.getRunwayAvailabilityStatus()){
            System.out.println("Flight "+this.name+" landed successfully");
            watchTower.setRunwayAvailabilityStatus(false); //flight is on the runway
        } else {
            System.out.println("Flight "+this.name+" is waiting for runway to be available");
        }
    }

    public void readyToLand(){
        System.out.println("Flight "+this.name+" is ready to land");
    }

    public void park(){
        System.out.println("Flight "+this.name+" is parked");
        watchTower.setRunwayAvailabilityStatus(true);
    }
}
