public interface WatchTowerInterface {

//    public void registerFlight(Flight flight);
//    public void registerRunway(Runway runway);

    public void setRunwayAvailabilityStatus(boolean status);

    public Boolean getRunwayAvailabilityStatus();
}
