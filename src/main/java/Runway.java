public class Runway implements Action{
    private WatchTowerInterface watchTower;

    public Runway(WatchTowerInterface watchTower){
        this.watchTower = watchTower;
    }

    @Override
    public void land(){
        System.out.println("Runway is available for landing");
        watchTower.setRunwayAvailabilityStatus(true);
    }
}

