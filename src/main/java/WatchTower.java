public class WatchTower implements WatchTowerInterface{
//    private Runway runway;
//    private Flight flight;
    private boolean availabilityStatus;// availability of the runway


//    @Override
//    public void registerFlight(Flight flight) {
//        this.flight = flight;
//    }
//
//    @Override
//    public void registerRunway(Runway runway) {
//        this.runway = runway;
//    }

    @Override
    public void setRunwayAvailabilityStatus(boolean status) {
        this.availabilityStatus = status;
    }

    @Override
    public Boolean getRunwayAvailabilityStatus() {
        return availabilityStatus;
    }
}
