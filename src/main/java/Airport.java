public class Airport {
    public static void main(String[] args) {
        //Airport preparation
        WatchTowerInterface watchTower = new WatchTower();
        Runway runway = new Runway(watchTower);
        Flight flight828 = new Flight(watchTower, "828");
        Flight flight321 = new Flight(watchTower, "321");

        //Registering runways and flights
//        watchTower.registerRunway(runway);
//        watchTower.registerFlight(flight828);
//        watchTower.registerFlight(flight321);

        //Flight actions, mediator(watchtower) is mediating
        flight828.readyToLand();
        runway.land();
        flight828.land();

        flight321.land();
        flight828.park();

        flight321.land();
        flight321.park();



    }
}
